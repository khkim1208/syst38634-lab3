package password;

/**
 * Author: Kanghyun Kim
 * Student No.: 991542281
 * Validates passwords and is being developed using TDD
 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_DIGIT = 2;
	
	/**
	 * 
	 * @param password
	 * @return true if number of characters is 8 or more. No spaces are allowed.
	 */	
	public static boolean isValidLength(String password) {
		
		return password.indexOf(" ") < 0 && password.length() >= MIN_LENGTH;
		
//		if(password.indexOf(" ") >= 0) {
//		return false;
//		}
//		return password.trim().length() >= MIN_LENGTH;
	}
	
	/**
	 * 
	 * @param password
	 * @return true if password has more than 2 digits
	 * and if number of characters is 8 or more. No spaces are allowed.
	 */	
	public static boolean isValidDigit(String password) {
		
		int count = 0;
		for(int i = 0; i < password.trim().length(); i++) {
			if(Character.isDigit(password.charAt(i))) {
				count++;
			}
		}
		return count >= MIN_DIGIT && isValidLength(password);
	}
	
	public static boolean hasValidCaseChars(String password) {
		return password != null && password.matches("^.*[a-z].*$") && password.matches("^.*[A-Z].*$");
	}


}
