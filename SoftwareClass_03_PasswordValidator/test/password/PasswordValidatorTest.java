package password;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Author: Kanghyun Kim
 * Student No.: 991542281
 */

public class PasswordValidatorTest {
	
	@Test
	public void testHasValidCaseCharsRegular() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("aBaaksiB"));
	}
	
	@Test 
	public void testHasValidCaseCharsException() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("783737"));
	}
	
	@Test 
	public void testHasValidCaseCharsExceptionSpecial() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("^%$#@"));
	}
	
	@Test 
	public void testHasValidCaseCharsExceptionBlank() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(""));
	}
	
	@Test 
	public void testHasValidCaseCharsExceptionNull() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars( null ));
	}
	
	@Test 
	public void testHasValidCaseCharsBoundaryIn() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("hT"));
	}
	
	@Test 
	public void testHasValidCaseCharsBoundaryOutUpper() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("BKKIYF"));
	}
	
	@Test 
	public void testHasValidCaseCharsBoundaryOutLower() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("aaaa"));
	}	
	
	
	
	@Test
	public void testIsValidLengthRegular() {
		boolean result = PasswordValidator.isValidLength("1234567890");
		assertTrue("Invalid Length", result );
	}
	
	@Test
	public void testIsValidLengthException() {
		boolean result = PasswordValidator.isValidLength("");
		assertFalse("Invalid Length", result );
	}	
	
	@Test
	public void testIsValidLengthExceptionSpaces() {
		boolean result = PasswordValidator.isValidLength("     t   e   s   t     ");
		assertFalse("Invalid Length", result );
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean result = PasswordValidator.isValidLength("12345678");
		assertTrue("Invalid Length", result );
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean result = PasswordValidator.isValidLength("1234567");
		assertFalse("Invalid Length", result );
	}
	
	
	
	@Test
	public void testIsValidDigitRegular() {
		boolean result = PasswordValidator.isValidDigit("abcde12345");
		assertTrue("Invalid Digit Requirement", result);
	}
	
	@Test
	public void testIsValidDigitException() {
		boolean result = PasswordValidator.isValidDigit("");
		assertFalse("Invalid Digit Requirement", result);
	}
	
	@Test
	public void testIsValidDigitExceptionSpaces() {
		boolean result = PasswordValidator.isValidDigit("  ab   cde12 34 56  ");
		assertFalse("Invalid Digit Requirement", result);
	}
	
	@Test
	public void testIsValidDigitBoundaryIn() {
		boolean result = PasswordValidator.isValidDigit("12abcdef");
		assertTrue("Invalid Digit Requirement", result);
	}
	
	@Test
	public void testIsValidDigitBoundaryOut() {
		boolean result = PasswordValidator.isValidDigit("1abcdefg");
		assertFalse("Invalid Digit Requirement", result);
	}
	
}


